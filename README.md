<div>
    <p align="center">
    <a href = "mailto:victor.alcantara@unionitdigital.com.br" target="_blank">
        <img src="https://img.shields.io/static/v1?label=Union IT Digital&message=E-mail&color=yellow&style=for-the-badge" alt="Author: Victor">
    </a>
    <a href="https://www.linkedin.com/in/victor-valencio-854012209/" target="_blank">
        <img src="https://img.shields.io/static/v1?label=Author&message=Victor Valencio&color=00ba6d&style=for-the-badge&logo=LinkedIn" alt="Author: Victor">
    </a>
    <a href="#">
		<img  src="https://img.shields.io/static/v1?label=Language&message=Java&color=red&style=for-the-badge&logo=Java"  alt="Language: Java">
	</a>
    </p>
</div>

## 📌About

<div>
    <p align="center">
Basicamente, hardcode são valores que estão no código-fonte, mas que deveriam vir de fontes externas (por exemplo, um arquivo de configuração).
    </p>
</div>

## 📕Exemplo de variável hardcode
```
String UrlDB = "https://www.payara.com/"url do seu banco de dados"";
```